#include <libpq-fe.h>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <gflags/gflags.h>
#include <chrono>
#include "../classes/Playlist.h"

//using namespace std;

DEFINE_uint64(duration, 0, "duration of playlist");
DEFINE_string(genre, "", "genre of track,quantity");
DEFINE_string(type, "M3U", "type of final file(s)");
DEFINE_string(subgenre, "", "subgenre of track,quantity");
DEFINE_string(artist, "", "artist of track,quantity");
DEFINE_string(album, "", "album,quantity");
DEFINE_string(title, "", "title,quantity");
DEFINE_string(name, "Playlist", "name of the playlist and file");

int main(int argc, char **agrv)
{
  ::google::ParseCommandLineFlags(&argc, &agrv, true);
  char con_info[] = "host=postgresql.bts-malraux72.net port=5432 user=t.lehugeur password=P@ssword dbname=Cours";//informations de connexion au$

  std::string db_host = "postgresql.bts-malraux72.net";
  std::string db_name = "Cours";
  std::string db_user = "t.lehugeur";
  std::string db_password = "P@ssword";
  int seconds = 0;
  
  PGconn *connexion = PQsetdbLogin(db_host.c_str(), "5432", nullptr, nullptr, db_name.c_str(), db_user.c_str(), db_password.c_str());

  std::chrono::duration<int> leTemps;
  Track newTrack = Track();
  std::vector<Track> lesTracks = newTrack.addTrack(FLAGS_title, FLAGS_genre, FLAGS_album, FLAGS_artist, FLAGS_duration, connexion);

 for(Track t : lesTracks)
  {
    seconds += t.getDuration();
  }

 leTemps = std::chrono::seconds(seconds);
  Playlist laPlaylist = Playlist(3, FLAGS_name, leTemps, lesTracks);
  laPlaylist.writeM3U();
  return 0;
}
