#ifndef GENRE_H
#define GENRE_H
#include <iostream>

class Genre
{
  //attributes
 private :
  unsigned int id;
  std::string type;

  //methods
 public :
  Genre();
  Genre(unsigned int id, std::string type);
  ~Genre();

  void setId(unsigned int);
  void setType(std::string);
  unsigned int getId();
  std::string getType();
};
#endif
