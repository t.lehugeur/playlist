#include "Format.h"
#include <iostream>

Format::Format() :
  id(0),
  entitled("")
{}

Format::Format(unsigned int _id, std::string _entitled) :
  id(_id),
  entitled(_entitled)
{}

Format::~Format() {}

void Format::setId(unsigned int _id)
{
  id = _id;
}

void Format::setEntitled(std::string _entitled)
{
  entitled = _entitled;
}

unsigned int Format::getId()
{
  return id;
}

std::string Format::getEntitled()
{
  return entitled;
}
