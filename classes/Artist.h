#ifndef ARTIST_H
#define ARTIST_H

#include <iostream>

class Artist
{

  //attributs

 private :
  unsigned int id;
  std::string name;

  //Constructeur

 public :
  Artist();
  Artist(unsigned int, std::string);
  ~Artist();

  //methodes
  void setName(std::string);
  void setId(unsigned int);
  std::string getName();
  unsigned int getId();
};
#endif
