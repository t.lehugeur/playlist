#ifndef CLI_H
#define CLI_H
#include <iostream>
#include <map>

class Cli
{

  //attributes
 private :
  unsigned int id;
  std::map<std::string, float> genre, subgenre;

  Cli();
  Cli(unsigned int, std::map<std::string, float>, std::map<std::string, float>);
  ~Cli();

  std::map<std::string, float> getGenre();
  std::map<std::string, float> getSubgenre();
  unsigned int getId();

  void setGenre(std::map<std::string, float>);
  void setSubgenre(std::map<std::string, float>);
  void setId(unsigned int);

};
#endif
