#ifndef PLAYLIST_H
#define PLAYLIST_H
#include <iostream>
#include "Track.h"
#include <chrono>
#include <vector>

class Playlist
{
  //attributes
 private :
  unsigned int id;
  std::string title;
  std::chrono::duration<int> duration;
  std::vector<Track> tracks;

  //methods
 public :
  Playlist();
  Playlist(unsigned int, std::string, std::chrono::duration<int>, std::vector<Track>);
  ~Playlist();

  void setId(unsigned int);
  void setTitle(std::string);
  void setDuration(std::chrono::duration<int>);
  void setTrack(std::vector<Track>);

  unsigned int getId();
  std::string getTitle();
  std::chrono::duration<int> getDuration();
  std::vector<Track> getTrack();


  //WriteM3U();
  //writeXSPF();
  /** \brief Write out the playlist to M3U file format
   *
   * The aim of this method is to write the playlist content to the
   * output file named "PLAYLIST" for the moment (waiting to use an
   * attribute with the playlist name)
   */
  void writeM3U();

  /** \brief Write out the playlist to XSPF file format
   *
   * Same as Playlist::writeM3U but the output format file is XSPF,
   * using the \c libxspf library.
   */
  void writeXSPF();

};



#endif
