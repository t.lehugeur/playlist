#include "Subgenre.h"
#include <iostream>

Subgenre::Subgenre() :
  id(0),
  subtype("")
{}

Subgenre::Subgenre(unsigned int _id, std::string _subtype) :
  id(_id),
  subtype(_subtype)
{}

Subgenre::~Subgenre() {}

void Subgenre::setId(unsigned int _id)
{
  id = _id;
}

void Subgenre::setSubtype(std::string _subtype)
{
  subtype = _subtype;
}

unsigned int Subgenre::getId()
{
  return id;
}

std::string Subgenre::getSubtype()
{
  return subtype;
}
