#include "Playlist.h"
#include <iostream>
#include <fstream>

Playlist::Playlist() :
  id(),
  title(),
  duration(),
  tracks() {}
Playlist::Playlist(unsigned int _id, std::string _title, std::chrono::duration<int> _duration, std::vector<Track>_tracks) :
  id(_id),
  title(_title),
  duration(_duration),
  tracks(_tracks) {}

Playlist::~Playlist() {};

void Playlist::setId(unsigned int _id)
{
  id = _id;
}
void Playlist::setTitle(std::string _title)
{
  title = _title;
}
void Playlist::setDuration(std::chrono::duration<int> _duration)
{
  duration = _duration;
}
void Playlist::setTrack(std::vector<Track> _tracks)
{
  tracks = _tracks;
}

unsigned int Playlist::getId()
{
  return id;
}
std::string Playlist::getTitle()
{
  return title;
}
std::chrono::duration<int> Playlist::getDuration()
{
  return duration;
}
std::vector<Track> Playlist::getTrack()
{
  return tracks;
}

//writeM3U()
//writeXSPF

void Playlist::writeM3U()
{
  std::string filename = title + ".m3u";
  {
    std::ofstream m3u_stream(filename, std::ios::out);

    if(m3u_stream.is_open())
    {
      m3u_stream << "#EXTM3U\n\n";
      for(Track current_track : tracks)
      {
        m3u_stream << "#EXTINF:" << current_track.getDuration() << ", " << current_track.getAnArtist().getName() << " - " << current_track.getName() << "\n" << current_track.getPath() << "\n\n";
      }
    }
  }
}

void Playlist::writeXSPF()
{}
