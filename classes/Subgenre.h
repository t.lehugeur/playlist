#ifndef SUBGENRE_H
#define SUBGENRE_H
#include <iostream>

class Subgenre
{
 private:
  unsigned int id;
  std::string subtype;

 public:
  Subgenre();
  Subgenre(unsigned int, std::string);
  ~Subgenre();


  void setId(unsigned int);
  void setSubtype(std::string);
  unsigned int getId();
  std::string getSubtype();


};
#endif
